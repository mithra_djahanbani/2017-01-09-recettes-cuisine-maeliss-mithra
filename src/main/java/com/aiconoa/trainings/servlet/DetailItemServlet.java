package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.aiconoa.trainings.entities.Recipe;

/**
 * Servlet implementation class DetailItemServlet
 */
@WebServlet("/DetailItemServlet")
public class DetailItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(DetailItemServlet.class.getName());
	
	@Resource(lookup = "java:jboss/DataSources/RecipeDS")
	private static DataSource recipeDS;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String index = request.getParameter("index");
		int ind = Integer.parseInt(index)+1;
				
		try (Connection conn = recipeDS.getConnection()){
			
			String sql = "SELECT * FROM recipe.recipe WHERE id = ? ";
			
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setInt(1 , ind);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			Recipe recipe = null;
			while (resultSet.next()) {
				LOGGER.info("recupere base de donnees");
				String title = resultSet.getString("title");
				String thumbNailUrl = resultSet.getString("thumbNailUrl");
				String imageUrl = resultSet.getString("imageUrl");
				recipe = new Recipe(title, thumbNailUrl, imageUrl);
			}
			
			request.setAttribute("recipe", recipe);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/recipe.jsp");
			requestDispatcher.forward(request, response);

		} catch (SQLException e1) {
			LOGGER.log(Level.SEVERE, "Can't communicating with the database", e1);
			response.sendError(500, "Something wrong happend, please contact the support");
			return;
		}
	}
}
