package com.aiconoa.trainings.entities;

public class Recipe {

	private String title;
	private String thumbNailUrl;
	private String imageUrl;
	
	public Recipe(String title, String thumbNailUrl, String imageUrl) {
		super();
		this.title = title;
		this.thumbNailUrl = thumbNailUrl;
		this.imageUrl = imageUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getThumbNailUrl() {
		return thumbNailUrl;
	}

	public void setThumbNailUrl(String thumbNailUrl) {
		this.thumbNailUrl = thumbNailUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
}
